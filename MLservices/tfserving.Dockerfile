FROM tensorflow/serving:2.1.0
CMD ["--rest_api_port=8501", "--model_config_file=/models/config.conf", "--rest_api_timeout_in_ms=120000"]

WORKDIR /models

COPY storage/KerasModel /models


