# Bases image for backend and celery worker
FROM continuumio/miniconda3:4.7.12

LABEL maintainer="Amrest Chinkamol"
LABEL version="0.1.1a"

WORKDIR /tmp
COPY environment.yml /tmp/

# Building
RUN apt-get autoclean \
    && apt-get update \
    && apt-get install -y \
    gcc \
    g++ \
    cmake \
    && apt-get clean \
    && conda env create --name fade --file=environment.yml \
    && conda clean --all -f \
    && echo "source activate fade" > ~/.bashrc
