# FACERECOG UTILITY FUNCTION

import os
import random
import shutil
from typing import Any, List, Tuple, Sequence

from pathlib import Path

import cv2
import imutils
from loguru import logger

import numpy as np

from config import (
    AllowImageType,
    AllowVideoType,
    settings)
from exceptions import LabelNotExists


# Helper

def csv_name_template(key) -> str:
    return f'facial_class-{key}.csv'


def data_sampler(source_path: Path,
                 max_size: int = 100) -> Sequence[Any]:
    """
    Sample k samples data from source path
    """
    # Count file in dir
    count = 0
    n_samp = max_size
    _temp = []
    for path in source_path.iterdir():
        if path.is_file():
            if AllowImageType.has(path.suffix):
                count += 1
                _temp.append(str(path))
    if max_size > count:
        n_samp = count
    return random.choices(_temp, k=n_samp)


def model_name_template(key) -> str:
    return f'representations-{key}.pkl'


def get_face_position(image) -> Sequence[Any]:

    net = cv2.dnn.readNetFromCaffe(settings.FACE_POS_DEPLOY_TXT,
                                   settings.FACE_POS_WEIGHT)
    (h, w) = image.shape[:2]
    blob = cv2.dnn.blobFromImage(cv2.resize(image, (300, 300)), 1.0,
                                 (300, 300), (104.0, 177.0, 123.0))

    net.setInput(blob)
    detections = net.forward()

    face_index = []
    for i in range(0, detections.shape[2]):
        confidence = detections[0, 0, i, 2]
        if confidence > 0.8:
            face_index.append(i)
    pos = []

    for i in face_index:

        box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
        (startX, startY, endX, endY) = box.astype("int")
        pos.append((startX, startY, endX, endY))

    return pos


def remove_ifempty_file(fp) -> None:
    if os.path.getsize(fp) == 0:
        logger.warning("UNEXPECTED | Remove empty file")
        os.remove(fp)
        return True
    else:
        return False


def extract_face_from_video(video_path: str,
                            video_index: str,
                            person_name: str,
                            storage_path: Path) -> None:

    stream = cv2.VideoCapture(video_path)
    p_name = person_name

    storage_path.joinpath(p_name)
    count = 0
    success_count = 0
    img_count = 0
    success = False
    while (not success):
        (grabbed, frame) = stream.read()

        if not grabbed:
            break
        frame = imutils.resize(frame, width=1200)
        # overlay = frame.copy()

        face_postions = get_face_position(frame)
        if len(face_postions) > 1:
            continue

        # loop over the face detections
        for rect in face_postions:
            # convert the dlib rectangle into an OpenCV bounding box and
            # draw a bounding box surrounding the face
            (startX, startY, endX, endY) = rect

            # train_face = frame[startY:endY, startX:endX]
            # w = int(0.3*(endX-startX))
            # h = int(0.3*(endY-startY))

            # startX = startX-w
            # endX = endX+w
            # startY = startY-h
            # endY = endY+h

            face = frame[startY:endY, startX:endX]

            face_name = "{}_{}_{}.{}".format(
                person_name, video_index, img_count, 'jpg')
            save_path = storage_path.joinpath(face_name)
            try:
                if count % 4 == 0:
                    # logger.debug(f"Write to {str(save_path)}")
                    cv2.imwrite(str(save_path), face)
                    img_count += 1
                    success_count += 1
                count += 1
                if img_count > 100:
                    success = True
            except Exception as e:
                logger.warning(f'Exception caught and ignored {e}.')
                continue

    logger.info(f"Finished obtaining image from {video_path} total \
                {success_count}")


def data_processor(source_labellist: List[Path],
                   process_storage: Path) -> None:
    logger.info(f"Processing {len(source_labellist)} label(s).")
    """
    params:
        source_labellist: List of Path to each corresponding label's video dir
        processing_storage: Where should an processed image be.
    """
    # Iterate through each corresponding label's video dir.
    total = len(source_labellist)
    labelsbar = enumerate(source_labellist)
    for i, labelpath in labelsbar:
        # Get label from dirpath
        label = labelpath.stem
        # Check if target corresponding label's processed dir exists.
        if not process_storage.joinpath(label).is_dir():
            if not process_storage.joinpath(label).parent.is_dir():
                os.makedirs(str(process_storage))
            process_storage.joinpath(label).mkdir()

        # Processing each video in dir
        labelbar = enumerate(labelpath.iterdir())
        yield {'label': label,
               'index': i,
               'total': total,
               'file': '',
               'status': 'reading'}
        for index, file_path in labelbar:
            # Check if it is a file and it is a properly format video
            if file_path.is_file() and AllowVideoType.has(file_path.suffix):
                yield {
                    'label': label,
                    'index': i,
                    'total': total,
                    'file': file_path.stem,
                    'status': 'working',
                }
                extract_face_from_video(str(file_path),
                                        index,
                                        label,
                                        process_storage.joinpath(label),)
            else:
                # Someone do something within an upload dir. Should not happen.
                logger.warning(f"ANOMALY | {file_path} should not exists")
                logger.warning(f"ANOMALY | suffixes {file_path.suffix}")
            # yield {
            #         'label': label,
            #         'index': i,
            #         'total': total,
            #         'file': file_path.stem,
            #         'status': 'done',
            #     }
    logger.info(f"Finished Processing {len(source_labellist)} label(s)")


def loader(labels: List[str],
           source_dir: Path,
           output_path: Path,
           use_existed: bool = False,
           use_existed_path: Path = None) -> Tuple[List[Path], List[str]]:
    """
    params:
    labels: list of person's name, literal.
    source_dir: Path of directory which contain user-uploaded video
                storage | <- this path
                        - label_a
                          |
                           - a1.mp4
    output_path: Path of processed output
                output_path
                        |- label_a
                        |- label_b
    use_existed: Bool, skip the processing step. For testing.
    use_existed_path: Path, for processed test.
    Description:
        Train policy: Only allow 1 train dataset instance,
                    will remove an old instance if called again.
                    Except for `use_existed`,
                    trainer will use old instance instead.
        get label -> preprocessing -> train -> ??? -> profit
    Return
    - a list of processed label directory path.
    - a list of labels.
    """

    # Check if label video path is a directory
    source_labels_path: List[Path] = []

    logger.debug(f"Loading {len(labels)} ...")
    # Iterate through labels
    for label in labels:
        # Get label dirpath
        p_label = source_dir.joinpath(label)
        # Check if existed.
        try:
            if not use_existed:  # Skip checking
                assert p_label.is_dir()
        except AssertionError:
            logger.debug(f"{p_label} not exists. Terminate ...")
            raise LabelNotExists(label)
        # Append label path to loading list
        source_labels_path.append(p_label)     # Append existing label

    # Create train data directory
    try:
        # NOTE: Not tested if such a label exits yet
        # Create temporary train directory
        # NOTE: Need to add user-identifier to directory name
        storage_path = output_path
        # Prepare dirpath for creation
        if not use_existed:
            processing_path = \
                [storage_path.joinpath(label) for label in labels]
        if use_existed:
            processing_path = \
                [use_existed_path.joinpath(label) for label in labels]
        # If instance exists, delete.
        if not use_existed:
            if storage_path.is_dir():
                shutil.rmtree(storage_path)
        if not storage_path.is_dir():
            storage_path.mkdir()
        # Uses existed instance instead of processing again
        if use_existed:
            try:
                for _tpath in processing_path:
                    assert _tpath.is_dir()
            except AssertionError:
                logger.error(f"Label {_tpath.stem} not exists.")
    except FileNotFoundError:
        logger.debug("Error during making train directory ...")
        raise FileNotFoundError

    # Processing from source video
    meta = dict()
    if not use_existed:
        for meta in data_processor(source_labels_path, storage_path):
            meta['status'] = 'loading'
            yield meta
    meta['status'] = 'done'
    logger.info("Loading complete")

    # Return processed data dirpath for train function
    meta['data'] = processing_path, labels
    yield meta
