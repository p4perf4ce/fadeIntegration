import math
from pathlib import Path
import pickle
from typing import List

from loguru import logger

import cv2
import numpy as np
from services.restful_caller import prediction_call
from services.FaceRecog.util import data_sampler
from config import settings


def train(
    label_storage: Path,
    labels: List[str],
    output_model_path: Path,
    batch_size=256,
):
    """
    Params:
    datapath:Path dataset dirpath,
    labels: List of label need to train,
    output_model_path: dirpath for output assets,
    batch_size: simultaneous prediction size, larger the longer. (Beware of timeout).
    """
    names = []
    test_label = []
    test_path = []
    test_image = []
    for label in labels:
        label_path = label_storage.joinpath(label)
        names.append(label_path)
        _tmp = data_sampler(source_path=label_path, max_size=400)
        test_path = [*test_path, *_tmp]
        test_label = [*test_label, *[label for _ in range(len(_tmp))]]
    for p in test_path:                             #
        img = cv2.imread(p)                         #
        norm_image = cv2.resize(img, (160, 160))    # Optimizable (read -> predict -> free -> read -> ...)
        test_image.append(norm_image)               #
    logger.info('Creating model representations ...')
    img_array = np.array(test_image)/255.0
    logger.info('Running model ...')
    representatives = []
    total = int(math.ceil(len(img_array)/batch_size))
    logger.info(f'Total of {total} batches')
    for index in range(0, len(img_array), batch_size):
        _i = (index//batch_size) + 1
        logger.info(f'Running {_i}/{total} batch')
        repr_batch = prediction_call(
            target=settings.faceurl,
            data=img_array[index: index + batch_size],
        )
        logger.info(f'Finished {_i}/{total} batch')
        representatives.extend(repr_batch)  # O(k) times where k = len(repr_batch)
    representations = []
    logger.info('Saving model ...')
    representations.append([representatives, test_label, names])
    with output_model_path.open('wb') as modelfile:
        pickle.dump(representations, modelfile)
    logger.info(f'Model saved to {str(output_model_path)}')
