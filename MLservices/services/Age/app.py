from services.Age.age_predict import predict_age
from utils import s3_helper

import os
from json import dumps, loads
from kafka import KafkaProducer
from kafka import KafkaConsumer

# log
import logging
logger = logging.getLogger(__name__)
handler = logging.StreamHandler()
handler.setFormatter(logging.Formatter(
    '%(asctime)s - %(name)s - %(message)s'))
logger.addHandler(handler)
logger.setLevel(logging.INFO)

# Get Environment variables
KAFKA_HOST = os.environ['KAFKA_HOST']
KAFKA_PORT = os.environ['KAFKA_PORT']
KAFKA_TOPIC_IMAGE = os.environ['KAFKA_TOPIC_IMAGE']
KAFKA_TOPIC_AGE_RESULT = os.environ['KAFKA_TOPIC_AGE_RESULT']

# display environment variable
logger.info('KAFKA_HOST: {}'.format(KAFKA_HOST))
logger.info('KAFKA_PORT: {}'.format(KAFKA_PORT))
logger.info('KAFKA_TOPIC_IMAGE: {}'.format(KAFKA_TOPIC_IMAGE))
logger.info('KAFKA_TOPIC_AGE_RESULT: {}'.format(KAFKA_TOPIC_AGE_RESULT))


def main():
    # TODO: Kafka Config
    consumer = KafkaConsumer(KAFKA_TOPIC_IMAGE,
                             bootstrap_servers=[
                                 '{}:{}'.format(KAFKA_HOST, KAFKA_PORT)],
                             auto_offset_reset='earliest',
                             enable_auto_commit=True,
                             group_id='age-detection-group')

    producer = KafkaProducer(
        bootstrap_servers=['{}:{}'.format(KAFKA_HOST, KAFKA_PORT)])

    logger.info('Ready for consuming messages')
    for message in consumer:
        # de-serialize
        input_json = loads(message.value.decode('utf-8'))
        logger.info('Input JSON: {}'.format(dumps(input_json, indent=2)))

        # Get image from S3
        img_stream = s3_helper.get_file_stream_s3(
            input_json['image_path'])

        # inference
        predicted, detail = predict_age(img_stream)
        formatted = {'age': predicted, 'detail': detail}

        # Response
        age_result = {'image_id': input_json['image_id'],
                      **formatted}
        logger.info('Age Result JSON: {}'.format(
            dumps(age_result, indent=2)))

        # Send to Kafka
        producer.send(KAFKA_TOPIC_AGE_RESULT,
                      value=dumps(age_result).encode('utf-8'))


if __name__ == '__main__':
    main()
