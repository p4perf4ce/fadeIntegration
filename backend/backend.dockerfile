FROM registry.gitlab.com/p4perf4ce/truelabfade:base-image-0.1.1a

ARG CONDA_DIR=/opt/conda
ARG ENVNAME=fade

ENV PATH $CONDA_DIR/bin:$PATH

WORKDIR /app
COPY . /app
ENV PYTHONPATH=/app


RUN chmod +x ./backend-start.sh \
    && echo "source activate $ENVNAME" > ~/.bashrc

CMD ["bash", "./backend-start.sh"]