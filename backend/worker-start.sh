#! /bin/bash
set -e
source activate fade
echo "Waiting for MQ service to start ..."
sleep 5

python /app/$WORKER_DIR/celery_prestart.py

celery worker -A $WORKER_DIR.interfaces -l info -Q main-queue -c 1
